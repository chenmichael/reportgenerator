ARG TOOL_VERSION=TOOL_VERSION_UNSET
ARG FRAMEWORK_VERSION=9.0

FROM --platform=${BUILDPLATFORM} mcr.microsoft.com/dotnet/sdk:${FRAMEWORK_VERSION}-alpine AS build
WORKDIR /tmp

# Install global tool
ARG TOOL_VERSION
RUN git clone --depth 1 --branch "v${TOOL_VERSION}" https://github.com/danielpalme/ReportGenerator.git repo

ARG FRAMEWORK_VERSION
RUN dotnet publish --self-contained false --framework "net${FRAMEWORK_VERSION}" --configuration Release --output tools/dotnet-reportgenerator repo/src/ReportGenerator.Console.NetCore
RUN test -f tools/dotnet-reportgenerator/ReportGenerator.dll || echo "ReportGenerator.dll not found!"
RUN printf "#!/bin/sh -e\ndotnet /opt/bin/dotnet-reportgenerator/ReportGenerator.dll \"\$@\"\n" > tools/reportgenerator && chmod +x tools/reportgenerator

FROM mcr.microsoft.com/dotnet/runtime:${FRAMEWORK_VERSION}-alpine AS publish

# Copy installed tool to runtime image
ENV PATH="/opt/bin:${PATH}"
COPY --from=build /tmp/tools /opt/bin
CMD [ "reportgenerator" ]
